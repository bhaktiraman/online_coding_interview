from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from .forms import SignUpForm
from django.utils.safestring import mark_safe
import json

def index(request):
    return render(request, 'index.html',{})

def home(request):
    return render(request, 'home.html',{})

def login(request):
    return render(request, 'login.html',{})

def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'registration/register.html',{'form': form})

def interview(request):
    return render(request, 'interview.html',{})

def chat(request,room_name):
    return render(request, 'chat.html', {'room_name_json': mark_safe(json.dumps(room_name)), 'room_name':room_name})