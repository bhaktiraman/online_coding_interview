from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('home/', views.home, name='home'),
    path('register/', views.register, name='register'),
    path('interview/', views.interview, name='interview'),
    path('chat/<room_name>', views.chat, name='chat'),
]